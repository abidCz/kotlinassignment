In this project I am using Model View ViewModel architecture along with dagger2 and programing language is kotlin.
In this project there are four directories-
1. di- this dir contain dagger implementation which includes component and module(dependency provider)
2. models - general kotlin data classes
3. repos - Data repository which is responsible to fetch data from remote.
4. ViewModels - viewmodels are responsble to request datarepository for data and provide livedata to views.
the flow for api request is - view ask viewmodel for data and viewmodel ask data repos for the same. ViewModel notify views with the help of livedata.
 
