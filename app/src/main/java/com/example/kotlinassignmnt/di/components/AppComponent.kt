package com.example.kotlinassignmnt.di.components

import com.example.kotlinassignmnt.BaseApplication
import com.example.kotlinassignmnt.di.ActivityBuilder
import com.example.kotlinassignmnt.di.modules.AppModule
import com.example.kotlinassignmnt.di.modules.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


/**
 * Created by abid on 14,September,2019
 */
@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, AppModule::class, ActivityBuilder::class, ViewModelModule::class])
internal interface AppComponent : AndroidInjector<BaseApplication> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<BaseApplication>()
}