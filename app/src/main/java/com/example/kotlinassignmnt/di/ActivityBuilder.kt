package com.example.kotlinassignmnt.di

import com.example.kotlinassignmnt.PhotosViewActivity
import com.example.kotlinassignmnt.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by abid on 14,September,2019
 */
@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector
    abstract fun provideMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun providePhotoViewActivity(): PhotosViewActivity
}