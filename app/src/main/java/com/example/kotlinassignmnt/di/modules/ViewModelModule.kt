package com.example.kotlinassignmnt.di.modules

import dagger.Module
import androidx.lifecycle.ViewModel
import dagger.multibindings.IntoMap
import dagger.Binds
import com.example.kotlinassignmnt.viewModels.DaggerViewModelFactory
import androidx.lifecycle.ViewModelProvider
import com.example.kotlinassignmnt.viewModels.MainActivityVM
import com.example.kotlinassignmnt.viewModels.PhotosViewModel


/**
 * Created by abid on 14,September,2019
 */

@Module
abstract class ViewModelModule{
    @Binds
    internal abstract fun bindViewModelFactory(factory: DaggerViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityVM::class)
    internal abstract fun provideMainActivityViewModel(mainActivityVM: MainActivityVM): ViewModel
    @Binds
    @IntoMap
    @ViewModelKey(PhotosViewModel::class)
    internal abstract fun providePhotoViewModel(photosViewModel: PhotosViewModel): ViewModel
}