package com.example.kotlinassignmnt.di.modules

import com.example.kotlinassignmnt.ApiInterface
import com.example.kotlinassignmnt.repos.DataRepository
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

import javax.inject.Singleton
import okhttp3.logging.HttpLoggingInterceptor

/**
 * Created by abid on 14,September,2019
 */

@Module
class AppModule {
    private val BASE_URL:String="https://dog.ceo/"

    @Provides
    @Singleton
    fun provideGson(): Gson {
         return GsonBuilder().create()
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder().addInterceptor(interceptor).build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun provideApiInterface(retrofit: Retrofit):ApiInterface{
        return retrofit.create(ApiInterface::class.java)
    }

    @Provides
    @Singleton
    fun provideDataRepository(apiInterface: ApiInterface): DataRepository {
        return DataRepository(apiInterface)
    }




}