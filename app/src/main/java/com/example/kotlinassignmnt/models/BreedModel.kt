package com.example.kotlinassignmnt.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by abid on 14,September,2019
 */

class BreedModel {
    @SerializedName("message")
    @Expose
   public var result: Map<String, List<String>>? = null
}