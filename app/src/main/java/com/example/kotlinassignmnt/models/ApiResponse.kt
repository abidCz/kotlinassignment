package com.example.kotlinassignmnt.models

/**
 * Created by abid on 14,September,2019
 */
open class ApiResponse
data class ResponseSuccess<T> constructor(val response: T) :ApiResponse()
data class ResponseFailed constructor(val errorMsg: String) :ApiResponse()
