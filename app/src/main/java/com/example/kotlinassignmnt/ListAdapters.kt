package com.example.kotlinassignmnt

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.breed_list_item.view.*
import kotlinx.android.synthetic.main.breed_photo_view.view.*

/**
 * Created by abid on 14,September,2019
 */

const val BREED_NAME:String="breed_name"
class BreedListAdapter(
    private val context: Context,
    val breedData: Map<String, List<String>>
) : RecyclerView.Adapter<BreedItemViewHolder>() {

    var breedList:List<String> = breedData.keys.toList()
    private val layoutInflater:LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BreedItemViewHolder {
       var v:View= layoutInflater.inflate(R.layout.breed_list_item,parent, false)
        return BreedItemViewHolder(v)
    }

    override fun getItemCount(): Int {
        return breedData.size
    }

    override fun onBindViewHolder(holder: BreedItemViewHolder, position: Int) {
        holder.tvBreedName.text=breedList[position]
        var drawableId:Int=if(position%2==0) R.drawable.dummy1 else R.drawable.dummy2
        Glide.with(context).load(drawableId).into( holder.breedImageView)
        holder.breedImageView.setOnClickListener {
            var intent= Intent(context,PhotosViewActivity::class.java)
            intent.putExtra(BREED_NAME,breedList[position])
            context.startActivity(intent)
        }
    }

}

class BreedItemViewHolder(v:View): RecyclerView.ViewHolder(v) {
    var breedImageView:ImageView=v.breed_image
    var tvBreedName:TextView=v.tvBreedName

}

//Grid view Adapter
class GridPhotoViewAdapter(private val context: Context, val breedPhotos: List<String>): RecyclerView.Adapter<PhotoViewHolder>() {
    private val layoutInflater:LayoutInflater = LayoutInflater.from(context)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        var v:View= layoutInflater.inflate(R.layout.breed_photo_view,parent, false)
        return PhotoViewHolder(v)
    }

    override fun getItemCount(): Int {
        return breedPhotos.size
    }
    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        Glide.with(context).load(breedPhotos[position]).into( holder.imageView)

    }
}

class PhotoViewHolder(v:View):RecyclerView.ViewHolder(v){
    var imageView:ImageView=v.imageView
}

