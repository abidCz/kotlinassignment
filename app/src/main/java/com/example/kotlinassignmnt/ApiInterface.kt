package com.example.kotlinassignmnt

import com.example.kotlinassignmnt.models.BreedModel
import com.example.kotlinassignmnt.models.DogImagesList
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path


/**
 * Created by abid on 14,September,2019
 */
interface ApiInterface {
    @GET("/api/breed/{breed_name}/images")
    fun getBreedPhotos(@Path("breed_name")breed_name:String): Call<DogImagesList>

    @GET("/api/breeds/list/all")
    fun getBreedList(): Call<BreedModel>

}