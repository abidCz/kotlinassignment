package com.example.kotlinassignmnt.viewModels

import androidx.lifecycle.ViewModel
import com.example.kotlinassignmnt.repos.DataRepository
import javax.inject.Inject

/**
 * Created by abid on 14,September,2019
 */
class PhotosViewModel  @Inject constructor() : ViewModel() {

    @Inject
    lateinit var dataRepository: DataRepository

    public fun getBreedImages(breedName:String) {
        dataRepository.getBreedPhotos(breedName)
    }
    public fun get() = dataRepository.mutableLiveData
}