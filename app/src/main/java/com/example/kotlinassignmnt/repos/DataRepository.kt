package com.example.kotlinassignmnt.repos

import com.example.kotlinassignmnt.ApiInterface
import com.example.kotlinassignmnt.models.*
import com.example.kotlinassignmnt.viewModels.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by abid on 14,September,2019
 */
class DataRepository constructor(private val apiInterface: ApiInterface) {
    var mutableLiveData: SingleLiveEvent<ApiResponse>? = null
    init {
        mutableLiveData = SingleLiveEvent()
    }

    public fun getBreedList() {
        apiInterface.getBreedList().enqueue(object : Callback<BreedModel> {
            override fun onFailure(call: Call<BreedModel>, error: Throwable) {
                mutableLiveData!!.value = error.message?.let { ResponseFailed(it) }
            }
            override fun onResponse(call: Call<BreedModel>, response: Response<BreedModel>) {
                var breedModel: BreedModel? = response.body()
                mutableLiveData!!.value = (ResponseSuccess(breedModel))
            }
        })
    }

    public fun getBreedPhotos(breedName: String) {
        apiInterface.getBreedPhotos(breedName).enqueue(object : Callback<DogImagesList> {
            override fun onFailure(call: Call<DogImagesList>, error: Throwable) {
                mutableLiveData!!.value = error.message?.let { ResponseFailed(it) }
            }
            override fun onResponse(call: Call<DogImagesList>, response: Response<DogImagesList>) {
                var breedPhotosModel: DogImagesList? = response.body()
                mutableLiveData!!.value = ResponseSuccess(breedPhotosModel)
            }

        })
    }
}