package com.example.kotlinassignmnt

import com.example.kotlinassignmnt.di.components.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import androidx.lifecycle.ViewModel
import kotlin.reflect.KClass


/**
 * Created by abid on 14,September,2019
 */
class BaseApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication>? {
        return DaggerAppComponent.builder().create(this)
    }

}

