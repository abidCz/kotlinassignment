package com.example.kotlinassignmnt

import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.example.kotlinassignmnt.models.BreedModel
import com.example.kotlinassignmnt.models.DogImagesList
import com.example.kotlinassignmnt.models.ResponseFailed
import com.example.kotlinassignmnt.models.ResponseSuccess
import com.example.kotlinassignmnt.viewModels.DaggerViewModelFactory
import com.example.kotlinassignmnt.viewModels.MainActivityVM
import com.example.kotlinassignmnt.viewModels.PhotosViewModel
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

/**
 * Created by abid on 14,September,2019
 */
class PhotosViewActivity : DaggerAppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: DaggerViewModelFactory
    lateinit var viewModel: PhotosViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var breedName:String=intent.getStringExtra(BREED_NAME)
        supportActionBar?.title=breedName
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(PhotosViewModel::class.java)
        subscribeToViewModel()
        recyclerView.layoutManager= GridLayoutManager(this,2)
        recyclerView.setHasFixedSize(true)
        Handler().postDelayed(Runnable { viewModel.getBreedImages(breedName) },100)


    }

    private fun subscribeToViewModel() {
        viewModel.get()?.observe(this, Observer { result->
            run {
                if(result is ResponseSuccess<*> && result.response is DogImagesList){
                     var dogPhotosModel:DogImagesList= result.response
                    recyclerView.adapter= dogPhotosModel.let { dogPhotosModel.listData?.let { listData ->
                        GridPhotoViewAdapter(this,
                            listData
                        )
                    } }
                }else if(result is ResponseFailed){
                    Toast.makeText(application, result.errorMsg,Toast.LENGTH_LONG).show()
                }

            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId==android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }
    }
