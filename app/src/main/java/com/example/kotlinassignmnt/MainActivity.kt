package com.example.kotlinassignmnt

import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kotlinassignmnt.models.BreedModel
import com.example.kotlinassignmnt.models.ResponseFailed
import com.example.kotlinassignmnt.models.ResponseSuccess
import com.example.kotlinassignmnt.viewModels.DaggerViewModelFactory
import com.example.kotlinassignmnt.viewModels.MainActivityVM
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


/**
 * Created by abid on 14,September,2019
 */
class MainActivity : DaggerAppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: DaggerViewModelFactory
    lateinit var viewModel: MainActivityVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainActivityVM::class.java)
        subscribeToLiveData()
        viewModel.getBreedList()
        recyclerView.layoutManager = GridLayoutManager(this, 2)
        recyclerView.setHasFixedSize(true)
    }

    private fun subscribeToLiveData() {
        viewModel.get()?.observe(this, Observer { result ->
            run {
                if (result is ResponseSuccess<*> && result.response is BreedModel) {
                    var breedModel: BreedModel = result.response
                    var filteredValue = breedModel.result?.filterValues { it.isNotEmpty() }
                    recyclerView.adapter = filteredValue?.let { BreedListAdapter(this, it) }
                } else if (result is ResponseFailed) {
                    Toast.makeText(application, result.errorMsg, Toast.LENGTH_LONG).show()
                }
            }
        })
    }
}
